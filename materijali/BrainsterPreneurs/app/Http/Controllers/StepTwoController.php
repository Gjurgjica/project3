<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserRegistrationStep;

class StepTwoController extends Controller
{
    public function store()
    {
        UserRegistrationStep::where('user_id', Auth()->user()->id)->update(['redirectTo' => 'stepThree.index']);
        return redirect(route('stepThree.index'));
    }

    public function index()
    {
        return view('registration.step_two');
    }
}
