<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserRegistrationStep;

class StepOneController extends Controller
{
    public function store(Request $request)
    {
        UserRegistrationStep::where('user_id', Auth()->user()->id)->update(['redirectTo' => 'stepTwo.index']);
        return redirect(route('stepTwo.index'));
    }

    public function index()
    {
        return view('registration.step_one');
    }
}
