<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserRegistrationStep;

class StepThreeController extends Controller
{
    public function store()
    {
        UserRegistrationStep::where('user_id', Auth()->user()->id)->update(['redirectTo' => 'home']);
        return redirect(route('home'));
    }

    public function index()
    {
        return view('registration.step_three');
    }
}
