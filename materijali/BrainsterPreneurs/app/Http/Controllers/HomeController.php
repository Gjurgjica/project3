<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserRegistrationStep;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * We redirect user depending on which state is in registration process
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userRegisterStep = UserRegistrationStep::where('user_id', Auth()->user()->id)->first();
        if ($userRegisterStep->redirectTo == 'home') {
            return view('home');
        }

        return redirect(route($userRegisterStep->redirectTo));
    }
}
