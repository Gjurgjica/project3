<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRegistrationStep extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'redirectTo'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function user()
    {
        $this->belongsTo(Users::class, 'user_id');
    }
}
