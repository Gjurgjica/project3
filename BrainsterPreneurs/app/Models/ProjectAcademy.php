<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProjectAcademy extends Model
{
    protected $table = 'project_academies';

    protected $fillable =
        [
            'project_id',
            'academy_id'
        ];

    public $timestamps= false;

    public function academy()
    {
        $this->belongsTo(Academy::class, 'academy_id');
    }

    public function project()
    {
        $this->belongsTo(Project::class, 'project_id');
    }
}
