<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProjectMember extends Model
{
    protected $table = 'project_members';

    protected $fillable =
        [
            'project_id',
            'user_id',
            'message',
            'accepted'
        ];

    public $timestamps= false;
    protected $primaryKey = null;
    public $incrementing = false;

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function member()
    {
        return $this->belongsTo(User::class, 'user_id', );
    }
}
