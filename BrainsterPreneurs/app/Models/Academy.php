<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Academy extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    public function userAcademy(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(UserAcademy::class, 'id', 'academy_id');
    }
}
