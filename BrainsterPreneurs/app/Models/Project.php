<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';

    protected $fillable =
        [
            'name',
            'description',
            'user_id',
            'locked'
        ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function members()
    {
        return $this->belongsToMany(
            User::class,
            'project_members',
            'project_id',
            'user_id'
        );
    }

    public function academies()
    {
        return $this->belongsToMany(
            Academy::class,
            'project_academies',
            'project_id',
            'academy_id'
        );
    }

    public function projectMember()
    {
        return $this->hasMany(ProjectMember::class);
    }

    public function memberExists(User $user){
        return $this->members()->where('user_id', $user->id)->count() > 0;
    }
}
