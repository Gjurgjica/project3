<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class User extends Authenticatable implements HasMedia
{
    use HasApiTokens, HasFactory, Notifiable, InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'surname',
        'email',
        'password',
        'biography'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public static $MEDIA_COLLECTION = "user_image";

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function skills()
    {
        return $this->belongsToMany(
            Skill::class,
            UserSkill::class,
            'user_id',
            'skill_id');
    }

    public function academy()
    {
        return $this->hasOneThrough(
            Academy::class,
            UserAcademy::class,
            'user_id',
            'id'
        );
    }

    public function createdProjects()
    {
        return $this->hasMany(Project::class, 'user_id');
    }

    public function memberProjects()
    {
        return $this->belongsToMany(
            Project::class,
            'project_members',
            'user_id',
            'project_id'
        );
    }

    public function attachment()
    {
        return $this->getMedia(self::$MEDIA_COLLECTION);
    }

    public function hasPhoto()
    {
        return $this->attachment()->count() > 0;
    }
}
