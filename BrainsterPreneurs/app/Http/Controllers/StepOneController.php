<?php

namespace App\Http\Controllers;

use App\Models\Academy;
use App\Models\User;
use App\Models\UserAcademy;
use Illuminate\Http\Request;
use App\Models\UserRegistrationStep;

class StepOneController extends Controller
{
    public function store(Request $request)
    {
        if(optional(Auth()->user())->id == null)
            return redirect(route('login'));

        $user = UserAcademy::query()->where('user_id' , Auth()->user()->id)->first();
        if($user != null){
            $user->update(['academy_id' => $request->input('academy')]);
        }else{
            UserAcademy::create([
                'user_id' => Auth()->user()->id,
                'academy_id' => $request->input('academy')
                ]);
        }

        UserRegistrationStep::where('user_id', Auth()->user()->id)->update(['redirectTo' => 'stepTwo.index']);
        return redirect(route('stepTwo.index'));
    }

    public function index()
    {
        if(optional(Auth()->user())->id == null)
            return redirect(route('login'));

        $user = User::findOrFail(Auth()->user()->id);
        $academies = Academy::all();
        return view('registration.step_one', compact('academies', 'user'));
    }
}
