<?php

namespace App\Http\Controllers;

use App\Models\Academy;
use App\Models\Project;
use App\Models\ProjectMember;
use App\Models\User;
use App\Models\UserRegistrationStep;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index()
    {
        $userRegisterStep = UserRegistrationStep::where('user_id', Auth()->user()->id)->first();
        if ($userRegisterStep->redirectTo == 'home') {
            $user = User::findOrFail(Auth()->user()->id);
            $academies = Academy::all();
            $projects = $this->loadProjects($user, -1);
            $perPage = 4;
            $pageNumber = 1;
            $numberOfPages = ceil($projects->count() / $perPage);
            $projects = $projects->skip(($perPage * $pageNumber) - $perPage)->take($perPage);
            return view('home', compact('academies', 'projects', 'numberOfPages', 'pageNumber', 'user'));
        }

        return redirect(route($userRegisterStep->redirectTo));
    }

    public function getApplications(Request $request)
    {
        if(optional(Auth()->user())->id == null)
            return response()->json([],Response::HTTP_UNAUTHORIZED);

        $user = User::findOrFail(Auth()->user()->id);

        if($request->input('project_id') > 0){
            ProjectMember::create([
                'user_id' => $user->id,
                'project_id' => $request->input('project_id'),
                'message' => $request->input('message')
            ]);
        }


        $projects = $this->loadProjects($user, $request->get('academy_id'));
        $perPage = 4;
        $pageNumber = $request->get('page');

        $numberOfPages = ceil($projects->count() / $perPage);
        $projects = $projects->skip(($perPage * $pageNumber) - $perPage)->take($perPage);
        $isHome = 1;
        $html = view('_includes.home_cards', compact('projects', 'numberOfPages', 'pageNumber', 'user', 'isHome'))->render();

        return response()
            ->json(['html' => $html ],Response::HTTP_OK);
    }

    public function loadProjects($user, $filterAcademy){
        return Project::query()->where('projects.user_id','!=', $user->id)->where('projects.locked', 0)
            ->join('project_academies', 'project_academies.project_id', 'projects.id')
            ->where(function ($builder) use ($filterAcademy) {
                if ($filterAcademy != -1) {
                    $builder->where('project_academies.academy_id', $filterAcademy);
                }
            })->select('projects.*')->orderByDesc('created_at')->distinct()->get();
    }
}
