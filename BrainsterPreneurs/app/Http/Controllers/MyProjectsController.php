<?php


namespace App\Http\Controllers;


use App\Models\Academy;
use App\Models\Project;
use App\Models\ProjectAcademy;
use App\Models\ProjectMember;
use App\Models\Skill;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MyProjectsController extends Controller
{
    public function index()
    {
        if(optional(Auth()->user())->id == null)
            return redirect(route('login'));

        $user = User::findOrFail(Auth()->user()->id);

        return view('my_projects', compact('user'));
    }

    public function create()
    {
        if(optional(Auth()->user())->id == null)
            return redirect(route('login'));

        $user = User::findOrFail(Auth()->user()->id);
        $academies = Academy::all();

        return view('create_project', compact('user', 'academies'));
    }

    public function store(Request $request)
    {
        if(optional(Auth()->user())->id == null)
            return redirect(route('login'));

        $user = User::findOrFail(Auth()->user()->id);
        $request->merge(['user_id' => $user->id]);

        $project = Project::create($request->except('_token', 'academies'));

        $selectedAcademies = array_filter($request->get('academies', []), function($val){ return $val == "1"; });
        foreach ($selectedAcademies as $academy_id => $val){
            ProjectAcademy::create(['academy_id' => $academy_id, 'project_id' => $project->id]);
        }

        return redirect(route('my-projects.index'));
    }

    public function show($id)
    {
        if(optional(Auth()->user())->id == null)
            return redirect(route('login'));

        $project = Project::findOrFail($id);
        $user = User::findOrFail(Auth()->user()->id);

        return view('project-applicants', compact('project', 'user'));
    }

    public function edit($my_project)
    {
        if(optional(Auth()->user())->id == null)
            return redirect(route('login'));

        $project = Project::findOrFail($my_project);
        $user = User::findOrFail(Auth()->user()->id);
        $academies = Academy::all();

        return view('edit_project', compact('project', 'user', 'academies'));
    }

    public function update(Request $request, $my_project)
    {

        if(optional(Auth()->user())->id == null)
            return redirect(route('login'));

        $project = Project::findOrFail($my_project);
        $project->update($request->except(['_method', '_token', 'academies']));


        $project->academies()->sync([]);
        $selectedAcademies = array_filter($request->get('academies', []), function($val){ return $val == "1"; });
        foreach ($selectedAcademies as $academy_id => $val){
            ProjectAcademy::create(['academy_id' => $academy_id, 'project_id' => $project->id]);
        }

        return redirect(route('my-projects.index'));
    }

    public function destroy($my_project)
    {
        if(optional(Auth()->user())->id == null)
            return response()->json([],Response::HTTP_UNAUTHORIZED);

        ProjectAcademy::where('project_id', $my_project)->delete();
        ProjectMember::where('project_id', $my_project)->delete();
        Project::where('id', $my_project)->delete();

        return response()->json([],Response::HTTP_OK);
    }

    public function applicantAccept(Request $request)
    {
        if(optional(Auth()->user())->id == null)
            return response()->json([],Response::HTTP_UNAUTHORIZED);

        if($request->get('project_id') === null || $request->get('user_id') === null)
            return response()->json([],Response::HTTP_BAD_REQUEST);

        ProjectMember::where('project_id', $request->get('project_id'))
            ->where('user_id',$request->get('user_id'))->update(['accepted' => 1]);

        $projectMember = ProjectMember::where('project_id', $request->get('project_id'))
            ->where('user_id',$request->get('user_id'));

        $project = Project::findOrFail($request->get('project_id'));

        $cardHtml = view('_includes.project-applicants-card', compact('projectMember', 'project'))->render();

        return response()
            ->json(['html' => $cardHtml ],Response::HTTP_OK);
    }

    public function applicantProfile($user, $project)
    {
        if(optional(Auth()->user())->id == null)
            return redirect(route('login'));

        $user = User::findOrFail($user);
        $project = Project::findOrFail($project);

        return view('user_profile', compact('user', 'project'));
    }

    public function projectComplete(Request $request)
    {
        if(optional(Auth()->user())->id == null)
            return response()->json([],Response::HTTP_UNAUTHORIZED);
        $project = Project::findOrFail($request->get('project_id'));
        $project->update(['locked' => true]);

        return response()->json([],Response::HTTP_OK);
    }
}
