<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserSkill;
use Illuminate\Http\Request;
use App\Models\UserRegistrationStep;
use App\Models\Skill;

class StepTwoController extends Controller
{
    public function store(Request $request)
    {
        if(optional(Auth()->user())->id == null)
            return redirect(route('login'));

        $user = User::findOrFail(Auth()->user()->id);
        $selectedSkills = array_filter($request->get('skills', []), function($val){ return $val == "1"; });
        UserSkill::query()->where('user_id', $user->id)->delete();
        foreach ($selectedSkills as $skill_id => $val){
            UserSkill::create(['skill_id' => $skill_id, 'user_id' => $user->id]);
        }

        UserRegistrationStep::where('user_id', Auth()->user()->id)->update(['redirectTo' => 'stepThree.index']);
        return redirect(route('stepThree.index'));
    }

    public function index()
    {
        if(optional(Auth()->user())->id == null)
            return redirect(route('login'));

        $skills = Skill::all();
        $user = User::findOrFail(Auth()->user()->id);
        return view('registration.step_two', compact('skills', 'user'));
    }
}
