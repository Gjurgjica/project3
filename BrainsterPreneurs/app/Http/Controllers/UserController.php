<?php


namespace App\Http\Controllers;


use App\Models\User;

class UserController extends Controller
{
    public function getAvatar()
    {
        $user = User::findOrFail(optional(Auth()->user())->id);
        $media = $user->attachment()[0];
        return response()->file($media->getPath());
    }

    public function getAvatarByUser(User $user)
    {
        $media = $user->attachment()[0];
        return response()->file($media->getPath());
    }
}
