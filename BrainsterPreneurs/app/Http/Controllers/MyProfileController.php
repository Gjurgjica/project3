<?php


namespace App\Http\Controllers;


use App\Models\Skill;
use App\Models\User;
use App\Models\UserSkill;
use Illuminate\Http\Request;

class MyProfileController extends Controller
{
    public function index()
    {
        if(optional(Auth()->user())->id == null)
            return redirect(route('login'));

        $user = User::findOrFail(Auth()->user()->id);
        $skills = Skill::all();

        return view('myProfile', compact('user', 'skills'));
    }

    public function store(Request $request)
    {
        if(optional(Auth()->user())->id == null)
            return redirect(route('login'));

        $user = User::findOrFail(Auth()->user()->id);
        $file = $request->file('image');
        if($file !== null){
            $user->clearMediaCollection(User::$MEDIA_COLLECTION);
            $user->addMedia($file)->toMediaCollection(User::$MEDIA_COLLECTION);
        }

        $selectedSkills = array_filter($request->get('skills', []), function($val){ return $val == "1"; });

        UserSkill::query()->where('user_id', $user->id)->delete();
        foreach ($selectedSkills as $skill_id => $val){
            UserSkill::create(['skill_id' => $skill_id, 'user_id' => $user->id]);
        }

        $user->update([
            'name' => $request->get('name'),
            'surname' => $request->get('surname'),
            'email' => $request->get('email'),
            'biography' => $request->get('biography')
        ]);

        return redirect(route('my-profile.index'));
    }
}
