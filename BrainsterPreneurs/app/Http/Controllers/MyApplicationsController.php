<?php


namespace App\Http\Controllers;


use App\Models\Project;
use App\Models\ProjectMember;
use App\Models\User;
use Illuminate\Http\Response;

class MyApplicationsController extends Controller
{

    public function index()
    {
        if(optional(Auth()->user())->id == null)
            return redirect(route('login'));

        $user = User::findOrFail(Auth()->user()->id);

        return view('myApplications', compact('user'));
    }

    public function destroy($my_application)
    {
        if(optional(Auth()->user())->id == null)
            return response()->json([],Response::HTTP_UNAUTHORIZED);

        $project = Project::findOrFail($my_application);
        $user = User::findOrFail(Auth()->user()->id);

        ProjectMember::where('project_id', $project->id)
            ->where('user_id',$user->id)->delete();

        return response()
            ->json([],Response::HTTP_OK);
    }
}
