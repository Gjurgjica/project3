<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\UserRegistrationStep;

class StepThreeController extends Controller
{
    public function store()
    {
        if(optional(Auth()->user())->id == null)
            return redirect(route('login'));

        UserRegistrationStep::where('user_id', Auth()->user()->id)->update(['redirectTo' => 'home']);
        return redirect(route('home'));
    }

    public function index()
    {
        if(optional(Auth()->user())->id == null)
            return redirect(route('login'));

        $user = User::findOrFail(Auth()->user()->id);
        return view('registration.step_three', compact('user'));
    }

    public function mediaStore(Request $request)
    {
        $user = User::findOrFail(Auth()->user()->id);
        $file = $request->file('image');

        if($file == null){
            return back();
        }

        $user->clearMediaCollection(User::$MEDIA_COLLECTION);
        $user->addMedia($file)->toMediaCollection(User::$MEDIA_COLLECTION);
        return redirect(route('stepThree.index'));
    }
}
