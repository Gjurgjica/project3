/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!**************************************!*\
  !*** ./resources/js/project-card.js ***!
  \**************************************/
function removeHandler(btn, evt) {
  var route = btn.dataset.route;
  window.axios["delete"](route).then(function (data) {
    if (data.status === 200) {
      var card = btn.closest('.cards-card');
      var parent = card.parentElement;
      parent.removeChild(card);

      if (parent.firstElementChild && !parent.firstElementChild.classList.contains('first')) {
        parent.firstElementChild.classList.add('first');
      }
    }
  })["catch"](function (err) {
    console.log(err);
  });
}

function cardDescription() {
  var descriptions = document.querySelectorAll('.card-content-description .card-text');
}

var text = document.getElementById("text");
var showText = document.getElementById("moreText");
var textButton = document.getElementById("textButton");
textButton.addEventListener("click", function (e) {
  if (showText.classList.contains("d-none")) {
    textButton.innerText = "Show Less";
  } else {
    textButton.innerText = "Show More";
  }

  showText.classList.toggle("d-none");
});
window.addEventListener('DOMContentLoaded', function (event) {
  var deleteButtons = document.querySelectorAll('.edit-delete-controls.delete-control');
  deleteButtons.forEach(function (btn) {
    btn.addEventListener('click', removeHandler.bind(null, btn));
  });
  var removeButtons = document.querySelectorAll('.edit-delete-controls.remove-control');
  removeButtons.forEach(function (removeBtn) {
    removeBtn.addEventListener('click', removeHandler.bind(null, removeBtn));
  });
});
/******/ })()
;