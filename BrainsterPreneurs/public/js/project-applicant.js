/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*******************************************!*\
  !*** ./resources/js/project-applicant.js ***!
  \*******************************************/
function makeRequest(project_id, user_id) {
  var data = {
    'project_id': project_id,
    'user_id': user_id
  };
  return window.axios.post('/home/acceptApplicant', data);
}

window.addEventListener('DOMContentLoaded', function (event) {
  var teamAssembledBtn = document.getElementById("team-assembledBtn");
  if (teamAssembledBtn.classList.contains('completed')) return;
  var buttons = document.querySelectorAll('.card-applicant-button');
  buttons.forEach(function (btn) {
    btn.addEventListener('click', function (evt) {
      if (teamAssembledBtn.classList.contains('completed')) return;
      var card = evt.target.parentElement;
      var project_id = parseInt(evt.target.dataset.projectid);
      var user_id = parseInt(evt.target.dataset.userid);
      makeRequest(project_id, user_id).then(function (data) {
        if (data.status === 200) {
          card.outerHTML = data.data.html;
        }
      })["catch"](function (error) {
        console.log(error);
      });
    });
  });
  teamAssembledBtn.addEventListener('click', function (evt) {
    if (teamAssembledBtn.classList.contains('completed')) return;
    var data = {
      'project_id': teamAssembledBtn.dataset.projectid
    };
    window.axios.post('/home/my-projects/project', data).then(function (data) {
      if (data.status !== 200) return;
      teamAssembledBtn.classList.add('completed');
    })["catch"](function (err) {
      console.log(err);
    });
  });
});
/******/ })()
;