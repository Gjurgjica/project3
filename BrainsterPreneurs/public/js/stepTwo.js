/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!**********************************************!*\
  !*** ./resources/js/registration/stepTwo.js ***!
  \**********************************************/
var maxActiveItem = 10;

function stepTwo() {
  var items = document.querySelectorAll('[data-skillcard]');
  items.forEach(function (itm) {
    itm.addEventListener("click", skillItemClicked.bind(null, itm));
  });
}

function skillItemClicked(obj, event) {
  if (!obj) {
    return;
  }

  if (getActiveItemCount() >= maxActiveItem && !obj.classList.contains('active')) return;
  var input = obj.getElementsByTagName('input')[0];

  if (input) {
    if (obj.classList.contains('active')) {
      input.value = 0;
    } else {
      input.value = 1;
    }
  }

  obj.classList.toggle('active');
}

function getActiveItemCount() {
  return document.querySelectorAll('[data-skillcard].active').length;
}

window.addEventListener('DOMContentLoaded', function (event) {
  stepTwo();
});
/******/ })()
;