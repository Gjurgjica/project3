/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!**********************************************!*\
  !*** ./resources/js/registration/stepOne.js ***!
  \**********************************************/
function stepOne() {
  var items = document.querySelectorAll('.academy-item');
  items.forEach(function (itm) {
    itm.addEventListener("click", academyItemClicked);
  });
}

function academyItemClicked(event) {
  itemAcademyActiveClear();
  var item = findParentByClass(event.target, "academy-item", 3);
  if (item) item.classList.add('active');
  var id = event.target.getAttribute("ed");
  if (id) document.getElementById("input-academy").value = id;
}

function itemAcademyActiveClear() {
  var items = document.querySelectorAll('.academy-item');
  items.forEach(function (itm) {
    itm.classList.remove('active');
  });
}

function findParentByClass(node, className, maxLevel) {
  if (maxLevel < 0) return null;
  if (node.classList.contains(className)) return node;
  if (node.parentNode) return findParentByClass(node.parentNode, className, maxLevel--);else return null;
}

window.addEventListener('DOMContentLoaded', function (event) {
  stepOne();
});
/******/ })()
;