/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!******************************!*\
  !*** ./resources/js/home.js ***!
  \******************************/
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Applications = /*#__PURE__*/function () {
  function Applications() {
    _classCallCheck(this, Applications);

    _defineProperty(this, "pageNumber", void 0);

    _defineProperty(this, "filterAcademy", void 0);

    _defineProperty(this, "applyToAcademy", void 0);

    _defineProperty(this, "message", void 0);

    this.pageNumber = 1;
    this.filterAcademy = -1;
    this.applyToAcademy = -1;
  }

  _createClass(Applications, [{
    key: "request",
    value: function request() {
      var data = {
        'page': this.pageNumber,
        'academy_id': this.filterAcademy,
        'project_id': this.applyToAcademy,
        'message': this.message
      };
      return window.axios.post('/home/applications', data);
    }
  }, {
    key: "setPageNumber",
    value: function setPageNumber(number) {
      this.pageNumber = number;
    }
  }, {
    key: "setFilter",
    value: function setFilter(id) {
      this.filterAcademy = id;
      this.setPageNumber(1);
    }
  }, {
    key: "setApplyToAcademy",
    value: function setApplyToAcademy(id, message) {
      this.applyToAcademy = id;
      this.message = message;
    }
  }]);

  return Applications;
}();

window.addEventListener('DOMContentLoaded', function (event) {
  registerToModalButton();
  attachEvents();
});
var applications = new Applications();
var openSuccessModal = false;

function clearFilter() {
  var filters = document.querySelectorAll('#academies-wrapper button');
  filters.forEach(function (btn) {
    btn.classList.remove('active');
  });
}

function reRenderCards(data) {
  applications.setApplyToAcademy(-1);
  if (data.status !== 200) return;
  if (openSuccessModal) successModalOpen();
  var cardWrapper = document.getElementById('cards-wrapper');
  cardWrapper.innerHTML = data.data.html;
  attachEvents();
}

function attachEvents() {
  var paging = document.querySelectorAll('#application-paging .page-item');
  paging.forEach(function (btn) {
    if (!btn.classList.contains('disabled')) btn.addEventListener('click', pageNumberChangeHandler);
  });
  var filters = document.querySelectorAll('#academies-wrapper button');
  filters.forEach(function (btn) {
    btn.addEventListener('click', filterChangeHandler);
  });
  var applyToProjectButtons = document.querySelectorAll('.row.cards-card .projectCard-btn');
  applyToProjectButtons.forEach(function (btn) {
    if (!btn.classList.contains('disabled')) btn.addEventListener('click', applyToProject);
  });
}

function pageNumberChangeHandler(evt) {
  var pageNumber = parseInt(evt.target.dataset.pagenumber);
  if (pageNumber === -2) pageNumber = applications.pageNumber + 1;
  if (pageNumber === -1) pageNumber = applications.pageNumber - 1;
  applications.setPageNumber(pageNumber);
  applications.request().then(reRenderCards);
}

function filterChangeHandler(evt) {
  var id = evt.target.dataset.id;
  clearFilter();
  evt.target.classList.add('active');
  applications.setFilter(parseInt(id));
  applications.request().then(reRenderCards);
}

function applyToProject(evt) {
  var btn = evt.target;
  if (!btn.dataset.projectid) return;
  cardModalOpen(btn.dataset.projectid);
}

function cardModalOpen(project_id) {
  var textArea = document.getElementById('card-modal-message');
  textArea.value = "";
  var cardModalContainer = document.getElementById('card-modal');
  cardModalContainer.setAttribute('data-projectId', project_id);
  var myModal = bootstrap.Modal.getInstance(cardModalContainer);
  if (!myModal) myModal = new bootstrap.Modal(cardModalContainer);
  myModal.show();
}

function successModalOpen() {
  openSuccessModal = false;
  var modal = document.getElementById('card-modal-success');
  var myModal = bootstrap.Modal.getInstance(modal);
  if (!myModal) myModal = new bootstrap.Modal(modal);
  myModal.show();
}

function cardModalClose() {
  var cardModalContainer = document.getElementById('card-modal');
  var projectId = cardModalContainer.getAttribute('data-projectId');
  var myModal = bootstrap.Modal.getInstance(cardModalContainer);
  if (myModal) myModal.hide();
  openSuccessModal = true;

  if (projectId) {
    var textArea = document.getElementById('card-modal-message');
    applications.setApplyToAcademy(parseInt(projectId), textArea.value);
    applications.request().then(reRenderCards);
  }
}

function successModalClose() {
  var cardModalContainer = document.getElementById('card-modal-success');
  var myModal = bootstrap.Modal.getInstance(cardModalContainer);
  if (myModal) myModal.hide();
}

function registerToModalButton() {
  var modalButton = document.getElementById('card-modal-send');
  modalButton.addEventListener('click', cardModalClose);
  var modalButtonSuccess = document.getElementById('card-modal-success');
  modalButtonSuccess.addEventListener('click', successModalClose);
}
/******/ })()
;