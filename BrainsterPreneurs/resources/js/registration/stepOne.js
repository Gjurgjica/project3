function stepOne(){
    let items = document.querySelectorAll('.academy-item');
    items.forEach(function(itm){
        itm.addEventListener("click", academyItemClicked);
    })
}

function academyItemClicked(event){
    itemAcademyActiveClear();
    let item = findParentByClass(event.target, "academy-item", 3)
    if(item)
        item.classList.add('active');

    let id = event.target.getAttribute("ed");
    if(id)
        document.getElementById("input-academy").value = id;
}

function itemAcademyActiveClear(){
    let items = document.querySelectorAll('.academy-item');
    items.forEach(function(itm){
        itm.classList.remove('active')
    })
}

function findParentByClass(node, className, maxLevel){
    if(maxLevel < 0)
        return null;
    if(node.classList.contains(className))
        return node;
    if(node.parentNode)
        return findParentByClass(node.parentNode, className, maxLevel--)
    else
        return null;
}





window.addEventListener('DOMContentLoaded', (event) => {
    stepOne();
});



