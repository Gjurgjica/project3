function removeHandler(btn, evt){
    let route = btn.dataset.route;

    window.axios.delete(route)
        .then(function(data){
            if(data.status === 200){
                let card = btn.closest('.cards-card');
                let parent = card.parentElement;
                parent.removeChild(card);
                if(parent.firstElementChild && !parent.firstElementChild.classList.contains('first')){
                    parent.firstElementChild.classList.add('first');
                }
            }
        })
        .catch(function(err){console.log(err)});
}

function cardDescription(){
    let descriptions = document.querySelectorAll('.card-content-description .card-text')
}
let text = document.getElementById("text");
let showText = document.getElementById("moreText");
let textButton = document.getElementById("textButton");
textButton.addEventListener("click", (e) => {
    if (showText.classList.contains("d-none")) {
        textButton.innerText = "Show Less";
    } else {
        textButton.innerText = "Show More";
    }
    showText.classList.toggle("d-none");
});


window.addEventListener('DOMContentLoaded', (event) => {

    let deleteButtons = document.querySelectorAll('.edit-delete-controls.delete-control');
    deleteButtons.forEach(function(btn){
        btn.addEventListener('click', removeHandler.bind(null, btn));
    })

    let removeButtons = document.querySelectorAll('.edit-delete-controls.remove-control');
    removeButtons.forEach(function(removeBtn){
        removeBtn.addEventListener('click', removeHandler.bind(null, removeBtn))
    })

});
