const maxActiveItem = 4;

function start(){
    let items = document.querySelectorAll('.academy-card');
    items.forEach(function(itm){
        itm.addEventListener("click", academyItemClicked);
    })
}

function academyItemClicked(event){

    let item = findParentByClass(event.target, "academy-card", 3)
    if(!item){
        return;
    }
    if(getActiveItemCount() >= maxActiveItem && !item.classList.contains('active'))
        return;

    let input = item.getElementsByTagName('input')[0];
    if(item.classList.contains('active')){
        console.log(input);
        input.value = 0;
    }else{
        input.value = 1;
        console.log(input);
    }

    item.classList.toggle('active');
    //
    // if(item.classList.add('active'))
    // item.classList.add('active');

    // let id = event.target.getAttribute("ed");
    // if(id)
    //     document.getElementById("input-academy").value = id;
}

function getActiveItemCount(){
    return document.querySelectorAll('.academy-card.active').length;
}

function findParentByClass(node, className, maxLevel){
    if(maxLevel < 0)
        return null;
    if(node.classList.contains(className))
        return node;
    if(node.parentNode)
        return findParentByClass(node.parentNode, className, maxLevel--)
    else
        return null;
}

window.addEventListener('DOMContentLoaded', (event) => {
    start();
});



