class Applications{

    pageNumber;
    filterAcademy;
    applyToAcademy;
    message;

    constructor() {
        this.pageNumber = 1;
        this.filterAcademy = -1;
        this.applyToAcademy = -1;
    }

    request(){
        const data = {
            'page': this.pageNumber,
            'academy_id': this.filterAcademy,
            'project_id': this.applyToAcademy,
            'message': this.message
        };
        return window.axios.post('/home/applications', data);
    }

    setPageNumber(number){
        this.pageNumber = number;
    }

    setFilter(id){
        this.filterAcademy = id;
        this.setPageNumber(1);
    }

    setApplyToAcademy(id, message){
        this.applyToAcademy = id;
        this.message = message;
    }
}

window.addEventListener('DOMContentLoaded', (event) => {
    registerToModalButton();
    attachEvents();
});

let applications = new Applications();
let openSuccessModal = false;

function clearFilter(){
    let filters = document.querySelectorAll('#academies-wrapper button');
    filters.forEach(function (btn){
        btn.classList.remove('active');
    });
}

function reRenderCards(data){
    applications.setApplyToAcademy(-1);

    if(data.status !== 200)
        return;

    if(openSuccessModal)
        successModalOpen();

    let cardWrapper = document.getElementById('cards-wrapper');
    cardWrapper.innerHTML = data.data.html;
    attachEvents();
}

function attachEvents(){
    let paging = document.querySelectorAll('#application-paging .page-item');
    paging.forEach(function (btn){
        if(!btn.classList.contains('disabled'))
            btn.addEventListener('click', pageNumberChangeHandler);
    });

    let filters = document.querySelectorAll('#academies-wrapper button');
    filters.forEach(function (btn){
        btn.addEventListener('click', filterChangeHandler);
    });

    let applyToProjectButtons = document.querySelectorAll('.row.cards-card .projectCard-btn');
    applyToProjectButtons.forEach(function(btn){

        if(!btn.classList.contains('disabled'))
            btn.addEventListener('click', applyToProject);
    });
}

function pageNumberChangeHandler(evt){
    let pageNumber = parseInt(evt.target.dataset.pagenumber);
    if(pageNumber === -2)
        pageNumber = applications.pageNumber + 1;
    if(pageNumber === -1)
        pageNumber = applications.pageNumber - 1;

    applications.setPageNumber(pageNumber);
    applications.request().then(reRenderCards);
}

function filterChangeHandler(evt){
    let id = evt.target.dataset.id;
    clearFilter();
    evt.target.classList.add('active');
    applications.setFilter(parseInt(id));
    applications.request().then(reRenderCards);
}

function applyToProject(evt){
    let btn = evt.target;
    if(!btn.dataset.projectid)
        return;
    cardModalOpen(btn.dataset.projectid);
}

function cardModalOpen(project_id){
    let textArea = document.getElementById('card-modal-message');
    textArea.value = "";

    let cardModalContainer = document.getElementById('card-modal');
    cardModalContainer.setAttribute('data-projectId', project_id);

    let myModal = bootstrap.Modal.getInstance(cardModalContainer);
    if(!myModal)
        myModal = new bootstrap.Modal(cardModalContainer);
    myModal.show();
}

function successModalOpen(){
    openSuccessModal = false;
    let modal = document.getElementById('card-modal-success');
    let myModal = bootstrap.Modal.getInstance(modal);
    if(!myModal)
        myModal = new bootstrap.Modal(modal);
    myModal.show();
}

function cardModalClose(){
    let cardModalContainer = document.getElementById('card-modal');
    let projectId = cardModalContainer.getAttribute('data-projectId')
    let myModal = bootstrap.Modal.getInstance(cardModalContainer);

    if(myModal)
        myModal.hide();

    openSuccessModal = true;

    if(projectId){
        let textArea = document.getElementById('card-modal-message');
        applications.setApplyToAcademy(parseInt(projectId), textArea.value);
        applications.request().then(reRenderCards);
    }
}

function successModalClose(){
    let cardModalContainer = document.getElementById('card-modal-success');
    let myModal = bootstrap.Modal.getInstance(cardModalContainer);

    if(myModal)
        myModal.hide();
}

function registerToModalButton(){
    let modalButton = document.getElementById('card-modal-send');
    modalButton.addEventListener('click', cardModalClose);

    let modalButtonSuccess = document.getElementById('card-modal-success');
    modalButtonSuccess.addEventListener('click', successModalClose);
}


