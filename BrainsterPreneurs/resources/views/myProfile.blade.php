@extends('layouts.main')

@include('layouts.navbar-pages', ['page'=> 3])

@section('main_content')

    <form method="post" action="{{route('my-profile.store')}}" enctype="multipart/form-data">
        @csrf
        <div class="row row-main">
            <div class="col-5 mt-3">
                <h4>My Profile</h4>
                <div class="d-flex mt-4">
                    <div class="d-flex flex-column">
                        <div class="d-flex">
                            @if(count($user->attachment()) > 0)
                                <img class="img-myProfile mr-5" src="{{route('current-user-avatar.get')}}" alt="" />
                            @else
                                <img class="img-myProfile mr-5" src="https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg" alt="" />
                            @endif
                            <div class="d-flex flex-column m-5">
                                <input class="font-weight-bold input-withoutBorder" type="text" name="name" id="" value="{{$user->name}}" />
                                <input class="font-weight-bold input-withoutBorder" type="text" name="surname" id="" value="{{$user->surname}}" />
                                <input class="font-weight-bold input-withoutBorder" type="email" name="email" id="" value="{{$user->email}}" />
                            </div>
                        </div>
                        <div>
                            <p><small>Click here to upload an image
                                    <input type="file" id="image" name="image" class="form-control opacity-0" style="cursor: pointer"></small></p>
                        </div>
                    </div>
                </div>
                <div class="">
                    <p class="font-weight-bold" style="color: #949d99;">Biography:</p>
                    <textarea class="font-weight-bold" style="color: #949d99;" name="biography" id="biography">{{$user->biography}}</textarea>
                </div>
            </div>

            <div class="col-7 mt-3">
                <h4>Skills:</h4>



                <div class="d-flex">
                    <div>
                        <div class="col-12 justify-content-center d-grid gap-3 square mb-5 mt-5">
                            <div class="wrapper-scroll">
                                @foreach ($skills as $skill)
                                    <div class="skill-card-myProfile col-lg-2 col-md-3 col-xs-6 mt-3 mr-2 @if($user->skills->where('id', $skill->id)->count() > 0) active @endif" data-skillcard>
                                        <div class="box font-xx-small">{{$skill->name}}</div>
                                        <input type="hidden" name="skills[{{$skill->id}}]" value="@if($user->skills->where('id', $skill->id)->count() > 0)1 @endif">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <button type="submit" class="color-darkgreen-background text-light float-right" style="margin-top: 150px;">EDIT</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@push('link-script')
    <script src="{{ asset('js/stepTwo.js') }}" defer></script>
@endpush
