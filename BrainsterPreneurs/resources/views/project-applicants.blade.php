@extends('layouts.main')

@include('layouts.navbar-pages', ['page'=> 1])

@section('main_content')
    <div class="row row-main">
        <div class="col-sm-12 col-md-9">
            <h3 class="mb-5">{{$project->name}}</h3>
            <h5>Choose your temmates<img src=""/></h5>
        </div>
        <div class="col-sm-12 col-md-3">
            <div class="font-weight-light text-secondary">Ready to start?</div>
            <div class="font-weight-light text-secondary">Click on the button below</div>
            <button id="team-assembledBtn" class="team-assembledBtn mt-2 @if($project->locked) completed @endif" data-projectid="{{$project->id}}">TEAM ASSEMBLE
                @if($project->locked) <i class="fas fa-check"></i> @endif</button>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-12">
            <div class="d-flex flex-row justify-content-center margin-top-applicants">
                @foreach($project->projectMember as $projectMember)
                    @include('_includes.project-applicants-card')
                @endforeach
            </div>
        </div>
    </div>
@endsection

@push('link-script')
    <script src="{{ asset('js/project-applicant.js') }}" defer></script>
@endpush
