@extends('layouts.main')

@include('layouts.navbar-pages', ['page'=> 2])

@section('main_content')

    <div class="row h-100 row-main">
        <div class="col-12" style="height: 50px">
            <h4>My Applications</h4>
        </div>

        <div class="col-12 mt-5" style="height: calc(100% - 95px)">
            <div class="my-project-cards">
                @foreach($user->memberProjects->sortByDesc('id') as $project)
                    @include('_includes.project-card', [
                                'project' => $project,
                                'showBadge' => 1,
                                'first' => $loop->index == 0 ? 1 : 0,
                                'className' => 'big',
                                'isEditable' => 1
                                ])
                @endforeach
            </div>
        </div>
    </div>
@endsection


@push('link-script')
    <script src="{{ asset('js/project-card.js') }}" defer></script>
@endpush
