@extends('layouts.app')



@section('content')
<main>
    <div class="container-fluid register-background-img">
        <div class="row">
            <div class="col-6 m-5">
                <div class="h1 font-weight-bold">Register</div>
            </div>
        </div>
        <div class="row">
            <form method="post" action="{{route('register')}}">

                @csrf
                <div class="col-12 m-5 border-width">
                    <div class="row mb-3">
                        <div class="col-5 mr-3">
                            <div class="form-group">
                                <input type="text" id="name" name="name" class="form-control font-weight-bold input-button-border" v-model="name" placeholder="Name" @if(old("name")) value={{old("name")}} @endif>
                                @if($errors->get('name'))
                                <div class="text-danger">
                                    {{$errors->get('name')[0]}}
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="form-group">
                                <input type="text" id="surname" name="surname" class="form-control font-weight-bold input-button-border" v-model="surname" placeholder="Surname" @if(old("surname")) value={{old("surname")}} @endif>
                                @if($errors->get('surname'))
                                    <div class="text-danger">
                                        {{$errors->get('surname')[0]}}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-5 mr-3">
                            <div class="form-group">
                                <input type="text" id="email" name="email" class="form-control font-weight-bold input-button-border" v-model="email" placeholder="Email" @if(old("email")) value={{old("email")}} @endif>
                                @if($errors->get('email'))
                                    <div class="text-danger">
                                        {{$errors->get('email')[0]}}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-5 mb-5">
                            <div class="form-group">
                                <input type="password" id="password" name="password" class="form-control font-weight-bold input-button-border" v-model="password" placeholder="Password"  @if(old("password")) value={{old("password")}} @endif>
                                @if($errors->get('password'))
                                    <div class="text-danger">
                                        {{$errors->get('password')[0]}}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="form-group" style="width: 100%">
                            <label for="biography" class="font-weight-bold text-muted">Biography</label>
                            <textarea type="text" id="biography" name="biography" class="form-control font-weight-bold input-button-border"  style="background: transparent; border:none; " v-model="biography" placeholder="Lorem ipsum..." @if(old("biography")) value={{old("biography")}} @endif></textarea>
                            @if($errors->get('biography'))
                                <div class="text-danger">
                                    {{$errors->get('biography')[0]}}
                                </div>
                            @endif
                        </div>
                    </div>
                    <button class="btn color-darkgreen-background text-light mt-3">Next</button>
                </div>
            </form>
        </div>
    </div>
</main>















<!-- <main class="sm:container sm:mx-auto sm:max-w-lg sm:mt-10">
    <div class="flex">
        <div class="w-full">
            <section class="flex flex-col break-words bg-white sm:border-1 sm:rounded-md sm:shadow-sm sm:shadow-lg">

                <header class="font-semibold bg-gray-200 text-gray-700 py-5 px-6 sm:py-6 sm:px-8 sm:rounded-t-md">
                    {{ __('Register') }}
                </header>

                <form class="w-full px-6 space-y-6 sm:px-10 sm:space-y-8" method="POST" action="{{ route('register') }}">
                    @csrf

                    <div class="flex flex-wrap">
                        <label for="name" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {{ __('Name') }}:
                        </label>

                        <input id="name" type="text" class="form-input w-full @error('name')  border-red-500 @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                        <p class="text-red-500 text-xs italic mt-4">
                            {{ $message }}
                        </p>
                        @enderror
                    </div>

                    <div class="flex flex-wrap">
                        <label for="email" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {{ __('E-Mail Address') }}:
                        </label>

                        <input id="email" type="email" class="form-input w-full @error('email') border-red-500 @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                        <p class="text-red-500 text-xs italic mt-4">
                            {{ $message }}
                        </p>
                        @enderror
                    </div>

                    <div class="flex flex-wrap">
                        <label for="password" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {{ __('Password') }}:
                        </label>

                        <input id="password" type="password" class="form-input w-full @error('password') border-red-500 @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                        <p class="text-red-500 text-xs italic mt-4">
                            {{ $message }}
                        </p>
                        @enderror
                    </div>

                    <div class="flex flex-wrap">
                        <label for="password-confirm" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {{ __('Confirm Password') }}:
                        </label>

                        <input id="password-confirm" type="password" class="form-input w-full" name="password_confirmation" required autocomplete="new-password">
                    </div>

                    <div class="flex flex-wrap">
                        <button type="submit" class="w-full select-none font-bold whitespace-no-wrap p-3 rounded-lg text-base leading-normal no-underline text-gray-100 bg-blue-500 hover:bg-blue-700 sm:py-4">
                            {{ __('Register') }}
                        </button>

                        <p class="w-full text-xs text-center text-gray-700 my-6 sm:text-sm sm:my-8">
                            {{ __('Already have an account?') }}
                            <a class="text-blue-500 hover:text-blue-700 no-underline hover:underline" href="{{ route('login') }}">
                                {{ __('Login') }}
                            </a>
                        </p>
                    </div>
                </form>

            </section>
        </div>
    </div>
</main> -->
@endsection
