@extends('layouts.main')


@include('layouts.navbar-pages', ['page'=> 1])

@section('main_content')
<div class="row h-100 row-main">
    <div class="col-12" style="height: 50px">
        <div class="fw-bold">Have a new idea to make the world better?</div>
    </div>
    <div class="col-12" style="height: 50px">
        <form method="GET" action="{{route('my-projects.create')}}" class="d-flex">
            <h3 class="m-0 p-0">Create a new project</h3>
            <button class="ml-3 button-outline-none" type="submit">
                <img class="my_projects_icon" src="{{ asset('images/icons/1.png') }}" alt="" />
            </button>
{{--            <a href=""><img class="my_projects_icon mx-2" src="{{route('')}}" alt="" /></a>--}}
        </form>
    </div>

    <div class="col-12 mt-5" style="height: calc(100% - 150px)">
        <div class="my-project-cards">
            @foreach($user->createdProjects->sortByDesc('id') as $project)
                @include('_includes.project-card', [
                            'project' => $project,
                            'showBadge' => 1,
                            'first' => $loop->index == 0 ? 1 : 0,
                            'className' => 'big',
                            'isEditable' => 1
                            ])
            @endforeach
        </div>
    </div>
</div>

@endsection

@push('link-script')
    <script src="{{ asset('js/project-card.js') }}" defer></script>
@endpush
