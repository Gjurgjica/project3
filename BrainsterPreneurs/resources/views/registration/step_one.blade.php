@extends('layouts.registration')

@section('title', 'Academies')

@section('form')

    <form method="post" action="{{route('stepOne.store')}}" class="row">
        <div class="col-12">
            <div class="row ml-5 mb-5 margin-left-steps">
                <div class="col-12">
                    <h5>Please select one of the academies listed below</h5>
                </div>
            </div>
        </div>

        <div class="offset-1 col-10 mb-5">
            @csrf
            <input id="input-academy" type="hidden" name="academy" value="{{optional($user->academy)->id}}">
                <div class="d-flex justify-content-center">
                @foreach($academies as $academy)
                    <div class="academy-item m-2 @if(optional($user->academy)->id == $academy->id) active @endif">
                        <div class="box" ed="{{$academy->id}}">{{$academy->name}}</div>
                    </div>
                @endforeach
                </div>

        </div>
        <div class="col-11 mt-5">
            <button type="submit" class="btn color-darkgreen-background text-light mt-3 float-right">Next</button>
        </div>
    </form>
@endsection

@push('link-script')
    <script src="{{ asset('js/stepOne.js') }}" defer></script>
@endpush
