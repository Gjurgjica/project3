@extends('layouts.registration')

@section('title', 'Your profile image')

@section('form')
    <div class="row">
        <form id="image-form" method="post" action="{{route('stepThree-media.store')}}" class="col-12" enctype="multipart/form-data">
            @csrf
            <div class="col-12">
                <div class="row justify-content-center">
                    <div class="form-group text-center" id="image-holder">
                        @if(count($user->attachment()) > 0)
                            <img class="upload-img" src="{{route('current-user-avatar.get')}}?t={{time()}}" >
                        @else
                            <img class="upload-img" src="https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg">
                        @endif

                        <label for="image"></label>
                        <input type="file" id="image" name="image" class="form-control opacity-0">
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="h3">Click here to upload an image</div>
                </div>
            </div>
        </form>

        <form method="post" action="{{route('stepThree.store')}}" class="col-12">
            @csrf
            <div class="col-12 d-flex justify-content-center mt-3">
                <button type="submit" class="btn color-darkgreen-background text-light font-weight-bold mt-3">FINISH</button>
            </div>
        </form>
    </div>
@endsection

@push('link-script')
    <script src="{{ asset('js/stepThree.js') }}" defer></script>
@endpush
