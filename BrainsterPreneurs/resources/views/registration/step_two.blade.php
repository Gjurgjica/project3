@extends('layouts.registration')

@section('title', 'Skills')

@section('sub-title', 'Please select your skills set')

@section('form')
    <form method="post" action="{{route('stepTwo.store')}}" class="row" >
        @csrf
        <div class="offset-1 col-10 justify-content-center d-grid gap-3 square mb-5" style="max-height: 400px;overflow-y: scroll; box-shadow: rgb(50 50 93 / 2%) 0px 30px 55px -58px inset, rgb(0 0 0 / 22%) 0px 18px 36px -15px inset" >
            <div class="wrapper" style="height: 100%; overflow: initial">
                @php
                    $userSkills = $user->skills->pluck('id')->toArray();
                @endphp
                @foreach ($skills as $skill)
                    <div class="skill-card col-lg-2 col-md-3 col-xs-6 m-3 @if($user->skills->where('id', $skill->id)->count() > 0) active @endif" style="height: 150px;" data-skillcard>
                        <div class="box">{{$skill->name}}</div>
                        <input type="hidden" name="skills[{{$skill->id}}]" value="@if($user->skills->where('id', $skill->id)->count() > 0)1 @endif">
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-12">
            <button type="submit" class="btn color-darkgreen-background text-light mt-3 float-right">Next</button>
        </div>
    </form>
@endsection


@push('link-script')
    <script src="{{ asset('js/stepTwo.js') }}" defer></script>
@endpush
