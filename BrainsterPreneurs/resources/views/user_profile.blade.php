@extends('layouts.main')

@include('layouts.navbar-pages', ['page'=> 1])

@section('main_content')
    <div class="row row-main" style="height: 100%">
        <div class="col-12 ps-5 mt-5" style="height: calc(100% - 50px)">
            <div class="d-flex justify-content-between h-100">
                <div class="d-flex flex-column h-100">
                    <div class="d-flex flex-wrap">
                        @if(count($user->attachment()) > 0)
                            <img src="{{route('user-avatar.get', ['user' => $user->id])}}" class="img-profile me-5" />
                        @else
                            <img src="https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg" class="img-profile me-5" />
                        @endif
                        <div class="d-flex flex-column ml-5 justify-content-center">
                            <h4 class="color-gray">Name: </h4>
                            <h3>{{$user->name . ' ' . $user->surname}}</h3>
                            <h4 class="color-gray">Contact:</h4>
                            <h3>{{$user->email}}</h3>
                        </div>
                    </div>

                    <form action="{{route('my-projects.show', ['my_project' => $project])}}" method="get" class="d-flex h-100 align-items-end">
                        <div>
                            <button class="text-light btn-green" style="margin: 0 0 50px 0">BACK</button>
                        </div>
                    </form>

                </div>
                <div class="d-flex flex-column">
                    <h4 class="color-gray">Biography:</h4>
                    <p class="color-gray">{{$user->biography}}</p>
                    <p class="align-self-end">Show more</p>
                    <h4 class="color-gray mt-5">Skills:</h4>
                    <div class="d-flex flex-wrap justify-content-center">
                        @foreach($user->skills as $skill)
{{--skills-user-projects--}}
                            <div class="skills-user-projects">
                                <div class="">{{$skill->name}}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
