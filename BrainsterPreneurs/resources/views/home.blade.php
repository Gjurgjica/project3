@extends('layouts.main')

@section('main_content')
    <div class="row pt-2 h-100">
        <div class="col-4">
            <div id="left-container">
                <div class="row ml-4">
                    <div class="col-12">
                        <div class="my-4 font-weight-bold">In what filed can you be amazing?</div>
                    </div>
                </div>

                <div class="row ml-4 mt-5">
                    <div class="col-10" id="academies-wrapper">
                        <div class="d-flex flex-wrap">
                            <button class="btnDesign mb-2 ml-2 active" data-id="-1">ALL</button>
                            @foreach($academies as $academy)
                                <button class="btnDesign mb-2 ml-2" data-id="{{$academy->id}}">{{$academy->name}}</button>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>


        {{--            DESIGN NA KARTICA POPRAVI--}}


        <div class="col-8 h-100 overflow-hidden ">
            <div class="row card-explanation">
                <div class="col-10" style="max-width: 81%;">
                    <div class="d-flex justify-content-end">
                        <img class="arrow-img" src="{{asset('images/icons/3.png') }}" alt="">
                        <div class="my-4 fw-bold">Checkout the latest projects</div>
                    </div>
                </div>
            </div>
            <div id="cards-wrapper" style="height: calc(100% - 140px);">
                @include('_includes.home_cards')
            </div>
        </div>
    </div>
@endsection

@push('link-script')
    <script src="{{ asset('js/home.js') }}" defer></script>
    <script src="{{ asset('js/project-card.js') }}" defer></script>
@endpush


