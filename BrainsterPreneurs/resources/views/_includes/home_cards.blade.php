<div class="cards">
    @foreach($projects as $project)
        <div style="margin: 0 100px 0 0; height: 210px;">
            @include('_includes.project-card', [
                        'project' => $project,
                        'showBadge' =>'0',
                        'first' => $loop->index == 0 ? 1 : 0,
                        'className' => '',
                        'isHome' => 1
                        ])
        </div>
    @endforeach
</div>
<nav aria-label="Page navigation example" style="margin-top: 8px; float: right">
    <ul class="pagination justify-content-center" id="application-paging">
        <li class="page-item @if($pageNumber==1) disabled @endif" data-pageNumber="-1">
            <a class="page-link" data-pageNumber="-1">Previous</a>
        </li>
        @for ($i = 1; $i <= $numberOfPages; $i++)
            <li class="page-item @if($pageNumber == $i) disabled @endif" data-pageNumber="{{ $i }}">
                <a class="page-link" data-pageNumber="{{ $i }}">{{ $i }}</a>
            </li>
        @endfor
        <li class="page-item @if($pageNumber==$numberOfPages) disabled @endif" data-pageNumber="-2">
            <a class="page-link" data-pageNumber="-2">Next</a>
        </li>
    </ul>
</nav>
