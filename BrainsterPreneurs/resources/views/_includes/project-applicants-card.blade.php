<div class="card text-center applicants-card ml-3 @if($projectMember->accepted == 1) accepted @endif">
    <div class="card-body">
        <form action="{{route('my-projects.profile', ['user'=> $projectMember->member->id, 'project' => $project])}}">
            <button type="submit">
                @if($projectMember->member->hasPhoto())
                    <img src="{{route('user-avatar.get', ['user' => $projectMember->member->id])}}" alt="" class="img-applicants" />
                @else
                    <img src="https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg" alt="" class="img-applicants" />
                @endif
            </button>
        </form>
        <h5 class="card-title">{{$projectMember->member->name}}</h5>
        <h6 class="card-subtitle mb-2 color-orange">profession: {{$projectMember->member->academy->name}}</h6>
        <div class="text-muted">{{$projectMember->member->email}}</div>
        <p class="card-text mt-3 mb-5 text-wrap">Message: {{$projectMember->message}}</p>
    </div>
    <div class="card-applicant-button position-absolute text-muted" data-projectid="{{$projectMember->project_id}}" data-userid="{{$projectMember->member->id}}">+</div>
</div>
