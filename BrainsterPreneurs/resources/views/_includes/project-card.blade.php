<div class="row cards-card @if(isset($first) && $first == '1') first @endif @if(isset($className)) {{$className}} @endif">
    <div class="col-12 mb-5">
        <div class="row m-0 cards-card--content">
            <div class="col-3 text-center">
                <div class="imgProjectCard-wrapper">
                    @if($project->owner->hasPhoto())
                        <img src="{{route('user-avatar.get', ['user' => $project->owner->id])}}"
                             alt=""/>
                    @else
                        <img src="https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg"
                             alt=""/>
                    @endif
                    <div>
                        <h6>{{$project->owner->name}}</h6>
                    </div>
                    <div>
                        <div class="text-orange">I'm a {{$project->owner->academy->name}}</div>
                    </div>
                </div>

                <div>
                    <small class="font-xx-small font-weight-normal">I'm looking for</small>
                    <div class="row p-o m-0 justify-content-center mt-2">
                        @foreach($project->academies as $academy)
                            <div class="half-circle">
                                <div>
                                    <p class="pt-1 fontSize">{{$academy->name}}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-9">
                @if(request()->path() == "home/my-projects")
                    <form action="{{route('my-projects.show', ['my_project' => $project])}}" method="get">
                        <button type="submit" class="d-flex flex-column align-items-center text-light projectCard-applicant-circle">
                            <p class="mt-2 mb-0">{{$project->members->count()}}</p>
                            <div>Applicants</div>
                        </button>
                    </form>
                @else
                    <div class="d-flex flex-column align-items-center text-light projectCard-applicant-circle">
                        <p class="mt-2 mb-0">{{$project->members->count()}}</p>
                        <div>Applicants</div>
                    </div>
                @endif
                <div class="card-block mt-3 card-content-description">
                    <h6 class="card-title">{{$project->name}}</h6>
                    <p class="card-text pb-0 mb-0" id="text" style="font-size: smaller">
                        @if(strlen($project->description) > 150)
                            {{substr($project->description, 0, 150)}}
                        @else
                            {{$project->description}}
                        @endif
                    </p>
                    @if(strlen($project->description) > 150)
                        <span id="moreText" class="d-none">@if(strlen($project->description) > 150) {{substr($project->description, 150)}} @endif</span>
                    @endif

                    <div class="d-flex w-100 justify-content-end">
                        <button class="" id="textButton"><a href="#">Show More</a></button>
                    </div>
                </div>
                @if(isset($showBadge) && $showBadge == '1' && request()->path() == "home/my-projects")
                    @if($project->locked == 1)
                        <div class= "badge">
                            <img src="{{asset('/images/icons/badge.png')}}" alt="" >
                        </div>
                    @endif
                @endif
                @if(request()->path() == "home" || (isset($isHome) && $isHome == 1))
                    <button data-projectid="{{$project->id}}"
                            class="btn bg-secondary text-light text-center projectCard-btn float-right mt-5 @if($project->memberExists($user)) disabled @endif"
                            type="button">I'M IN</button>
                @endif

                @if(request()->path() == "home/my-applications")
                    <div class="application-respond">
                        <div class="d-flex">
                            @if(optional($project->projectMember->where('user_id', $user->id)->first())->accepted == 1)
                                    <span class="accepted text-muted">Application accepted</span>
                                    <img class="ml-2 w-5 h-5" src="{{asset('/images/icons/5.png')}}">
                            @else
                                <span class="denied text-danger">Application denied</span>
                                <img class="ml-2 w-5 h-5" src="{{asset('/images/icons/6.png')}}">
                            @endif
                        </div>
                    </div>
                @endif
            </div>
        </div>

        @if(isset($isEditable) && $isEditable == '1')
            <div class="controls">
                @if(request()->path() == "home/my-projects")
                    <img class="edit-delete-controls mb-3 delete-control" src="{{asset('/images/icons/7.png')}}" data-route="{{route('my-projects.destroy', ['my_project' => $project])}}" alt="">
                    <a href="{{route('my-projects.edit', ['my_project' => $project])}}"><img class="edit-delete-controls" src="{{asset('/images/icons/8.png')}}" alt=""></a>
                @endif
                @if(request()->path() == "home/my-applications"
                        && optional($project->projectMember->where('user_id', $user->id)->first())->accepted == 0)
                    <img class="edit-delete-controls remove-control" src="{{asset('/images/icons/2.png')}}" alt="" data-route="{{route('my-applications.destroy', ['my_application' => $project])}}">
                @endif

            </div>
        @endif
    </div>
</div>
