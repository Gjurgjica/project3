@extends('layouts.main')

@include('layouts.navbar-pages', ['page'=> 1])

@section('main_content')

    <form method="POST" action="{{route('my-projects.update', ['my_project' => $project])}}">
        @method('PUT')
        @csrf
        <div class="row row-main">
            <div class="col-6">
                <div class="row my-5">
                    <div class=" col-10 ml-4 mb-5">
                        <h3>Edit Project</h3>
                    </div>
                    <div class="col-4 ml-4 mb-5">
                        <div class="form-group">
                            <input type="text" id="name" name="name" class="form-control font-weight-bold outline-none" placeholder="Name" value="{{$project->name}}"/>
                            <div class="text-danger"></div>
                        </div>
                    </div>

                    <div class="col-10 ml-4">
                        <div class="form-group">
                            <label for="description" class="text-muted font-weight-bold mb-2">Description of project</label>
                            <textarea type="text" id="description" name="description" class="form-control font-weight-bold input-button-border"  style="background: transparent; border:none; " v-model="biography" >{{$project->description}}</textarea>
                            <div class="text-danger"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-6">
                <div class="row my-5">
                    <div class="col-12 ml-4 mb-5">
                        <h3>What I need</h3>
                    </div>

                    <div class="row v-100 h-100">
                        <div class="col-12 v-100 h-100 d-flex flex-wrap">
                            @foreach($academies as $academy)
                                <div class="academy-card m-2  @if($project->academies->where('id', $academy->id)->count() > 0) active @endif">
                                    <div class="box" ed="{{$academy->id}}">{{$academy->name}}</div>
                                    <input type="hidden" name="academies[{{$academy->id}}]" value="@if($project->academies->where('id', $academy->id)->count() > 0)1 @endif">
                                </div>
                            @endforeach
                        </div>

                    </div>
                    <div class="offset-6 col-6 mt-5 mb-4">
                        <strong class="options-text-orange">Please select no more than 4 options</strong>
                    </div>

                    <div class="offset-7 col-3 mt-5">
                        <button class="btn color-darkgreen-background
                         text-light font-weight-bold" type="submit">EDIT</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('link-script')
    <script src="{{ asset('js/create_project.js') }}" defer></script>
@endpush



