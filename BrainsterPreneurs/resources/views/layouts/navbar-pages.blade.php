@section('navbar-pages')
    <a href="{{route('my-projects.index')}}" class="nav-item nav-link text-dark font-weight-bold @if($page=='1') active @endif">My Projects</a>
    <a href="{{route('my-applications.index')}}" class="nav-item nav-link text-dark font-weight-bold @if($page=='2') active @endif">My Applications</a>
    <a href="{{route('my-profile.index')}}" class="nav-item nav-link text-dark font-weight-bold  @if($page=='3') active @endif">My Profile</a>
@endsection
