@extends('layouts.app')
<main>
    <div class="container-fluid background-img-steps d-flex flex-column h-100">
        <div class="row h-100">
            <div class="col-12">
                <div class="row m-5 margin-left-steps">
                    <div class="col-4">
                        <h1 class="font-weight-bold">@yield('title')</h1>
                    </div>
                    <div class="col-8">
                        <hr style="border-top: 3px solid #dd7f1e; margin-top: 30px">
{{--                        <div style="border-width:2px; border-style:solid; border-color:black; margin-top: 30px"></div>--}}
                    </div>
                </div>

                @hasSection('sub-title')
                    <div class="row margin-left-steps">
                        <div class="col-12">
                            <h5 class="text-secondary mb-5">@yield('sub-title')</h5>
                        </div>
                    </div>
                @endif

            </div>

            <div class="col-12 h-100">
                @yield('form')
            </div>
        </div>
    </div>
</main>
