@extends('layouts.app')

@section('content')
<main class="h-100 w-100 background-img-steps">
    <nav class="navbar navbar-expand-lg navbar-light" style=" height: 50px; padding-top: 20px;">
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav">
                <a href="#" class="nav-item nav-link active"><h6 class="m-4 font-weight-bold text-dark">
                        BRAINSTER<span class="text-secondary">PRENEURS</span></h6>
                </a>
            </div>
            <div class="navbar-nav ml-auto">
                @yield('navbar-pages')
                <form method="GET" action="@if(request()->path() == "home"){{route('my-projects.index')}}@else {{route('home')}} @endif" style="align-self: center;display: flex;">
                    <button class="outline-profile-img">
                        <img class="home-page-image"
                             @if($user->hasPhoto())
                                src="{{route('current-user-avatar.get')}}?t={{time()}}"
                             @else
                                src="https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg"
                            @endif
                             >
                    </button>
                </form>

            </div>
        </div>
    </nav>
    <hr class="text-decoration-none" style="height:3px; background: #dd7f1e;">

    <div class="container-fluid dashboard">
        @yield('main_content')
    </div>


</main>

<div class="modal fade" id="card-modal" tabindex="-1" aria-labelledby="registerModal-label" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="registerLabel">Message</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-floating">
                    <textarea class="form-control" placeholder="Place your message here..." id="card-modal-message" style="height: 100px"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="card-modal-send" class="btn btn-primary">Send</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="card-modal-success" tabindex="-1" aria-labelledby="registerModal-label" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="registerLabel">Message</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <label>Your application was successfully submitted!</label>
            </div>
            <div class="modal-footer">
                <button type="button" id="card-modal-success" data-dismiss="modal" class="btn btn-primary">OK</button>
            </div>
        </div>
    </div>
</div>

@endsection
