@extends('layouts.app')


<main class="h-100 w-100 background-img-steps">
    <nav class="navbar navbar-expand-lg navbar-light" style=" height: 70px">
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav">
                <a href="#" class="nav-item nav-link active"><h6 class="m-4 font-weight-bold text-large">
                        BRAINSTER<span class="text-secondary">PRENEURS</span></h6>
                </a>
            </div>
            <div class="navbar-nav ml-auto">
                <img class="home-page-image mt-3"
                     src="https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg">
            </div>
        </div>
    </nav>
    <hr class="text-decoration-none" style="height:3px; background: #dd7f1e;">

    <div class="container-fluid dashboard">
        @yield('content')
    </div>
</main>
