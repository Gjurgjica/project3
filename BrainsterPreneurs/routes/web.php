<?php

use App\Http\Controllers\HomeController;
//use App\Http\Controllers\MyProjectsController;
//use App\Http\Controllers\StepOneController;
//use App\Http\Controllers\StepThreeController;
//use App\Http\Controllers\StepTwoController;
//use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::resource('/stepOne', StepOneController::class)->only(['index', 'store']);
Route::resource('/stepTwo', StepTwoController::class)->only(['index', 'store']);
Route::resource('/stepThree', StepThreeController::class)->only(['index', 'store']);
Route::post('/stepThree-media', StepThreeController::class . "@mediaStore")->name('stepThree-media.store');

Route::resource('/home/my-projects', MyProjectsController::class);

Route::get('/home/my-projects/profile/{user}/{project}', MyProjectsController::class . '@applicantProfile')->name('my-projects.profile');
Route::post('/home/acceptApplicant', MyProjectsController::class. '@applicantAccept');
Route::post('/home/my-projects/project', MyProjectsController::class. '@projectComplete');

Route::get('/user-avatar', UserController::class . '@getAvatar')->name('current-user-avatar.get');
Route::get('/user-avatar/{user}', UserController::class . '@getAvatarByUser')->name('user-avatar.get');

Route::resource('/home/my-applications', MyApplicationsController::class)->only(['index', 'destroy']);
Route::resource('/home/my-profile', MyProfileController::class);

Route::post('/home/applications', [HomeController::class, 'getApplications']);

