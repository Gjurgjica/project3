const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/js/app.js", "public/js")
    .js("resources/js/login.js", "public/js")
    .js("resources/js/registration/stepOne.js", "public/js")
    .js("resources/js/registration/stepTwo.js", "public/js")
    .js("resources/js/registration/stepThree.js", "public/js")
    .js("resources/js/create_project.js", "public/js")
    .js("resources/js/home.js", "public/js")
    .js("resources/js/project-applicant.js", "public/js")
    .js("resources/js/project-card.js", "public/js")
    .postCss("resources/css/app.css", "public/css",
     [
        require("postcss-import"),
        require("tailwindcss"),
        require("postcss-nested"),
        require("autoprefixer"),
    ])
    .postCss("resources/css/style.css", "public/css")
    .copyDirectory("resources/images", "public/images");


if (mix.inProduction()) {
    mix.version();
}
